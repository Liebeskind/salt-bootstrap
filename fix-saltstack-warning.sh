#WD_BLACK SN850X!/usr/bin/env bash
#

# set -uo pipefail
# trap 's=$?; echo "$0: Error on line "$LINENO": $BASH_COMMAND"; exit $s' ERR

#===============================================================================
err_exit() {
  echo
  echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" | grep -P --color ".+"
  echo "!!! ERROR: $1" | grep -P --color ".+"
  echo "!!!        Aborting script!" | grep -P --color ".+"
  echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" | grep -P --color ".+"
  echo
  exit 1
}


#===============================================================================
fix_saltstack_distutils_warning() {
  # NOTE: The fix is taken from here: https://github.com/s0undt3ch/salt/blob/1b7fac1599a50c970fdef49abca7151cb11bdcd0/salt/__init__.py
  

  #  sed '/^def __define_global_system_encoding_variable__.*:$/i # NOTE: The fix is taken from here: https://github.com/s0undt3ch/salt/blob/1b7fac1599a50c970fdef49abca7151cb11bdcd0/salt/__init__.py\n#       This fix is inserted via git/talentix/salt-bootstrap/install.sh\n# Filter the backports package UserWarning about being re-imported\nwarnings.filterwarnings(\n    "ignore",\n    message="Setuptools is replacing distutils.",\n    category=UserWarning,\n    module="_distutils_hack",\n)\n\n' /usr/lib/python3.10/site-packages/salt/__init__.py | less
  sed -i '/^def __define_global_system_encoding_variable__.*:$/i \
# NOTE: The fix is taken from here: https://github.com/s0undt3ch/salt/blob/1b7fac1599a50c970fdef49abca7151cb11bdcd0/salt/__init__.py \
#       This fix is inserted via git/talentix/salt-bootstrap/install.sh \
#       Filter the backports package UserWarning about being re-imported \
warnings.filterwarnings( \
    "ignore", \
    message="Setuptools is replacing distutils.", \
    category=UserWarning, \
    module="_distutils_hack", \
) \
 \
' /usr/lib/python3.10/site-packages/salt/__init__.py 

}

configure_saltstack() {
  echo
  echo "=== $FUNCNAME"

  echo "${hostname}" > /etc/salt/minion_id

  echo "    +++ Creating saltstack minion config path: /etc/salt/minion.d"
  mkdir /etc/salt/minion.d || err_exit "Failed to create /etc/salt/minion.d!"

  echo "    +++ Creating config file /etc/salt/minion.d/file-directory.conf"
  cat >/etc/salt/minion.d/file-directory.conf <<EOF
file_client: local


# Saltstack is configured to run in masterless mode.
# This requires some configs of the salt-master (/etc/salt/master)
# to be added to the saltstack client (/etc/salt/minion).

top_file_merging_strategy: same

file_roots:
  base:
    - /srv/salt/states

pillar_roots:
  base:
    - /srv/salt/pillars
EOF
  read -p ">>> Showing config file. Press ENTER to continue."
  cat /etc/salt/minion.d/file-directory.conf
  read -p ">>> Press ENTER to continue."

}



#===============================================================================
# MAIN
#===============================================================================
fix_saltstack_distutils_warning
# configure_saltstack
