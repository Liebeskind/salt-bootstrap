{% if pillar.bootstrap is defined %}
include:
  - bootstrap.bootctl
  - timezone
  - bootstrap.locale
  - repo
  - bootstrap.prepare-reboot
{% endif                         %}

