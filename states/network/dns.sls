{{ tplfile }}> Configure dns servers /etc/systemd/resolved.conf:
  file.replace:
    - name: /etc/systemd/resolved.conf
    - pattern: '^#DNS='
    - repl: 'DNS=9.9.9.9 1.1.1.1'

{{ tplfile }}> Enable systemd-resolved symlink 1:
  file.symlink:
    - name: /etc/systemd/system/dbus-org.freedesktop.resolve1.service
    - target: /usr/lib/systemd/system/systemd-resolved.service
    - force:: True

{{ tplfile }}> Enable systemd-resolved symlink 2:
  file.symlink:
    - name: /etc/systemd/system/multi-user.target.wants/systemd-resolved.service
    - target: /usr/lib/systemd/system/systemd-resolved.service
    - force:: True

