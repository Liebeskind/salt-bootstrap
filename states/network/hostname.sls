{% set hostname = grains.id %}

{{ tplfile }}> Create /etc/hosts:
  file.managed:
    - name: /etc/hosts
    - contents: |
        # Static table lookup for hostnames.
        # See hosts(5) for details.

        127.0.0.1 localhost.localdomain localhost
        ::1      localhost.localdomain localhost
        127.0.0.1 {{ hostname }}.localdomain {{ hostname }}
#    - mode: '0644'
    - unless: grep -q "{{ hostname }}" /etc/hosts


