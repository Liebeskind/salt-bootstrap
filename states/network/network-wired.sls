# Init artificial dict for passing variable value outside of the scope of a for loop
# https://fabianlee.org/2016/10/18/saltstack-setting-a-jinja2-variable-from-an-inner-block-scope/

{% set wired_network_interface = ({ 'name': 'UNKNOWN' })                           %}

# Get network interfaces from saltstack module
{% set network_interfaces = salt.network.interfaces()                              %}

# Loop through all interfaces.
# Test manually: salt-call network.interfaces or: salt-call network.interface enp1s0
{% for network_interface, data in network_interfaces.items()                       %}

# Identify and save wired network interface, saving to the artificial dict
{%   if network_interface.startswith('enp') or network_interface.startswith('eno') %}
{%     if wired_network_interface.update({'name': network_interface }) %} {% endif %}
{%   endif %}
{% endfor %}

{% set wired_network_interface_name = wired_network_interface.name                 %}


{{ tplfile }}> Create wired network config with static ip address /etc/systemd/network/20-wired.network:
  file.managed:
    - name: /etc/systemd/network/20-wired.network
    - source: salt://{{ slspath }}/files/etc/systemd/network/20-wired.network.{{ wired_network_interface_name }} 
#    - mode: '0644'
    - template: jinja
    - context:
        wired_network_interface_name: {{ wired_network_interface_name }}  
    - creates: /etc/systemd/network/20-wired.network

{{ tplfile }}> Disable ipv6 /etc/sysctl.d/40-ipv6.conf:
  file.managed:
    - name: /etc/sysctl.d/40-ipv6.conf
    - source: salt://{{ slspath }}/files/etc/sysctl.d/40-ipv6.conf
#    - mode: '0644'
    - template: jinja
    - context:
        wired_network_interface_name: {{ wired_network_interface_name }}  
    - creates: /etc/sysctl.d/40-ipv6.conf

{{ tplfile }}> Create missing folders for systemd-networkd symlinks:
   file.directory:
    - names:
      - /etc/systemd/system/sockets.target.wants
      - /etc/systemd/system/sysinit.target.wants
      - /etc/systemd/system/network-online.target.wants

{{ tplfile }}> Enable systemd-networkd symlink 1:
  file.symlink:
    - name: /etc/systemd/system/dbus-org.freedesktop.network1.service
    - target: /usr/lib/systemd/system/systemd-networkd.service
    - force:: True
    - requires:
      - {{ tplfile }}> Create missing folders for systemd-networkd symlinks

{{ tplfile }}> Enable systemd-networkd symlink 2:
  file.symlink:
    - name: /etc/systemd/system/multi-user.target.wants/systemd-networkd.service
    - target: /usr/lib/systemd/system/systemd-networkd.service
    - force:: True
    - requires:
      - {{ tplfile }}> Create missing folders for systemd-networkd symlinks

#    - user: root
#    - group: www-data
#    - dir_mode: '0770'
#    - file_mode: '0664'

{{ tplfile }}> Enable systemd-networkd symlink 3:
  file.symlink:
    - name: /etc/systemd/system/sockets.target.wants/systemd-networkd.socket
    - target: /usr/lib/systemd/system/systemd-networkd.socket
    - force:: True
    - requires:
      - {{ tplfile }}> Create missing folders for systemd-networkd symlinks

{{ tplfile }}> Enable systemd-networkd symlink 4:
  file.symlink:
    - name: /etc/systemd/system/sysinit.target.wants/systemd-network-generator.service
    - target: /usr/lib/systemd/system/systemd-network-generator.service
    - force:: True
    - requires:
      - {{ tplfile }}> Create missing folders for systemd-networkd symlinks

{{ tplfile }}> Enable systemd-networkd symlink 5:
  file.symlink:
    - name: /etc/systemd/system/network-online.target.wants/systemd-networkd-wait-online.service
    - target: /usr/lib/systemd/system/systemd-networkd-wait-online.service
    - force:: True
    - requires:
      - {{ tplfile }}> Create missing folders for systemd-networkd symlinks

