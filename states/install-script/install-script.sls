{{ sls }}> Copy {{ slspath }}:
  file.managed:
   - name: /root/install.sh
   - mode: '0755'
   - contents: |
       #!/usr/bin/env bash

       echo "Running script $0"

       # Clone salt repo
       rm -rf /srv/salt
       cd /srv
       git clone git@gitlab.com:talentix.ch/salt.git salt

       echo "Continue with: "
       echo " 1. Check backup mount path"
       echo " 2. Check restore symlinks: ls -ald /backup/resto*"
       echo " 3. cd /srv/salt"
       echo " 4. Set calt grain: salt-call --local grains.set user uri"
       echo " 4. Check states/top.sls"
       echo " 5. salt-call --local state.apply test=true"

