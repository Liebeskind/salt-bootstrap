{{ tplfile }}> Create symlink for sshd service:
  file.symlink:
    - name: /etc/systemd/system/multi-user.target.wants/sshd.service
    - target: /usr/lib/systemd/system/sshd.service

{{ tplfile }}> Permit PubkeyAuthentication in ssh_config:
  file.replace:
    - name: /etc/ssh/sshd_config
    - pattern: '^#PubkeyAuthentication yes'
    - repl: 'PubkeyAuthentication yes'

{{ tplfile }}> Permit PermitRootLogin in ssh_config:
  file.replace:
    - name: /etc/ssh/sshd_config
    - pattern: '^#PermitRootLogin prohibit-password'
    - repl: 'PermitRootLogin yes'

{{ tplfile }}> Permit PasswordAuthentication in ssh_config:
  file.replace:
    - name: /etc/ssh/sshd_config
    - pattern: '^#PasswordAuthentication no'
    - repl: 'PasswordAuthentication yes'

{{ tplfile }}> Modify /etc/systemd/resolved.conf:
  file.replace:
    - name: /etc/systemd/resolved.conf
    - pattern: '^#DNS='
    - repl: 'DNS=1.1.1.1 8.8.8.8'

{{ tplfile }}> Enable systemd-resolved symlink 1:
  file.symlink:
    - name: /etc/systemd/system/dbus-org.freedesktop.resolve1.service
    - target: /usr/lib/systemd/system/systemd-resolved.service
    - force:: True

{{ tplfile }}> Enable systemd-resolved symlink 2:
  file.symlink:
    - name: /etc/systemd/system/multi-user.target.wants/systemd-resolved.service
    - target: /usr/lib/systemd/system/systemd-resolved.service
    - force:: True

