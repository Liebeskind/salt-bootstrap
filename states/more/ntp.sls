{{ tplfile }}> Install ntp package:
  pkg.installed:
    - pkgs:
      - ntp

{% if pillar.bootstrap is defined %}
{{ tplfile }}> Create symlink for ntpd service:
  file.symlink:
    - name: /etc/systemd/system/multi-user.target.wants/ntpd.service
    - target: /usr/lib/systemd/system/ntpd.service

{% else %}
{{ tplfile }}> Assure ntpd service is running:
  service.running:
    - name: ntpd
    - enable: True
    - require:
      - {{ tplfile }}> Install ntp package
#      - pkg: ntp
{% endif %}
