{% if pillar.bootstrap is defined %}
include:
  - bootstrap.bootctl
  - bootstrap.timezone
  - bootstrap.locale
  - bootstrap.repo
  - bootstrap.prepare-reboot
{% else                          %}
  - ssh
  - network
{% endif                         %}
#  - base.ntp
#  - base.packages
#  - base.updates
#  - base.wheel
