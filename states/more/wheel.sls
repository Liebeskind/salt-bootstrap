sudo:
  pkg.installed

/etc/sudoers.d/wheel:
  file.managed:
    - user: root
    - group: root
    - mode: '0440'
    - contents: |
        %wheel ALL=(ALL) ALL
        Defaults passwd_timeout=0
