{% set boot_part      = salt.mount.active()['/boot']['device']            %}
{% set root_part      = salt.mount.active()['/']['device']                %}
{% set root_part_uuid = salt.disk.blkid(root_part)[root_part]['PARTUUID'] %}

# {{ tplfile }}> Debug root_partition:
#   cmd.run:
#     - name: "echo \"root partition: {{ root_part }}\""

# {{ tplfile }}> Debug root_part_uuid:
#   cmd.run:
#     - name: "echo \"root_part_uuid: {{ root_part_uuid }}\""

{{ tplfile }}> Install bootctl:
  cmd.run:
    - name: "bootctl install"
    - creates: /boot/EFI/BOOT/BOOTX64.EFI
#     - /boot/EFI/BOOT/BOOTX64.EFI

{{ tplfile }}> Create /boot/loader/loader.conf:
  file.managed:
    - name: /boot/loader/loader.conf
    - mode: '0755'
#    - mode: '0644'
    - contents: |
        default arch
        timeout 3
        editor 0
    - require:
      - {{ tplfile }}> Install bootctl

{{ tplfile }}> Create /boot/loader/entries/arch.conf:
  file.managed:
    - name: /boot/loader/entries/arch.conf
    - mode: '0755'
#    - mode: '0644'
    - contents: |
        title   Arch Linux
        linux   /vmlinuz-linux
        initrd  /initramfs-linux.img
        options root=PARTUUID={{ root_part_uuid }} rw
    - require:
      - {{ tplfile }}> Install bootctl


