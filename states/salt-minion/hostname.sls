{{ tplfile }}> Create /etc/hosts:
  file.managed:
    - name: /etc/hosts
    - mode: '0644'
    - contents: |
        # Static table lookup for hostnames.
        # See hosts(5) for details.
        127.0.0.1 localhost.localdomain localhost
        ::1      localhost.localdomain localhost
        127.0.0.1 talentix.localdomain talentix
    - creates: /etc/hosts


{{ tplfile }}> Configure /etc/salt/minion_id:
  file.managed:
    - name: /etc/salt/minion_id
    - mode: '0644'
    - contents: |
        talentix
    - creates: /etc/salt/minion_id

