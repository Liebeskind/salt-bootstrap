{{ tplfile }}> Configure /etc/salt/minion_id:
  file.managed:
    - name: /etc/salt/minion_id
    - mode: '0644'
    - contents: |
        talentix
    - creates: /etc/salt/minion_id

