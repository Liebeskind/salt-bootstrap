#== {{ tplfile }}> Enable sshd service:
#==   service.running:
#==     - name: sshd
#==     - enable: True

{{ tplfile }}> create symlink for sshd service:
  file.symlink:
    - name: /etc/systemd/system/multi-user.target.wants/sshd.service
    - target: /usr/lib/systemd/system/sshd.service

{{ tplfile }}> Permit PubkeyAuthentication in ssh_config:
  file.replace:
    - name: /etc/ssh/sshd_config
    - pattern: '^#PubkeyAuthentication yes'
    - repl: 'PubkeyAuthentication yes'

{{ tplfile }}> Permit PermitRootLogin in ssh_config:
  file.replace:
    - name: /etc/ssh/sshd_config
    - pattern: '^#PermitRootLogin prohibit-password'
    - repl: 'PermitRootLogin yes'

{{ tplfile }}> Permit PasswordAuthentication in ssh_config:
  file.replace:
    - name: /etc/ssh/sshd_config
    - pattern: '^#PasswordAuthentication yes'
    - repl: 'PasswordAuthentication yes'
    - bufsize: file

