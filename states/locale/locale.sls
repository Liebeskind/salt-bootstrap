# LOCALIZATION
{{ tplfile }}> Add locale de_CH.UTF-8:
  locale.present:
    - name: de_CH.UTF-8 UTF-8

{{ tplfile }}> Add locale en_DK.UTF-8 UTF-8:
  locale.present:
    - name: en_DK.UTF-8 UTF-8

{{ tplfile }}> Add locale en_US.UTF-8 UTF-8:
  locale.present:
    - name: en_US.UTF-8 UTF-8

{{ tplfile }}> Run locale-gen if /etc/locale.gen has changed:
  cmd.run:
   - name: "locale-gen"
   - unless: diff <(md5sum /etc/locale.gen) <(cat /etc/locale.gen.md5)

{{ tplfile }}> Check if /etc/locale.gen has changed:
  cmd.run:
   - name: "md5sum /etc/locale.gen > /etc/locale.gen.md5"
   - require:
      - {{ tplfile }}> Run locale-gen if /etc/locale.gen has changed
   - onchanges:
      - {{ tplfile }}> Run locale-gen if /etc/locale.gen has changed

{{ tplfile }}> Set default locale /etc/locale.conf:
  file.managed:
    - name: /etc/locale.conf
    - contents: |
        LC_NUMERIC="de_CH.UTF-8"
        LC_TIME="en_US.UTF-8"
        LC_COLLATE="en_US.UTF-8"
        LC_MONETARY="de_CH.UTF-8"
        LC_MESSAGES="en_US.UTF-8"
        LC_PAPER="de_CH.UTF-8"
        LC_NAME="en_US.UTF-8"
        LC_ADDRESS="de_CH.UTF-8"
        LC_TELEPHONE="de_CH.UTF-8"
        LC_MEASUREMENT="de_CH.UTF-8"
        LC_IDENTIFICATION="en_US.UTF-8"
        LC_ALL=
        TIME_STYLE="posix-long-iso"

